# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Learnist::Application.config.secret_key_base = '45504aade86cb4ad46fb9779f90bda796e13e0b24857fb7fd192bbf124a935f6f3ff3d6298453849df589ec5b427e73cfddf4031d6ca4b2a99fa0707361b7f42'
